runtime theme.vim
runtime user.vim

set number
set hidden
set incsearch
set hlsearch
set title
set tabstop=4
set shiftwidth=4
set clipboard+=unnamed,unnamedplus                        " use the system clipboard for yank/put/delete
set noerrorbells                                          " No Beeps.
let mapleader="\<SPACE>"

" Custom Shortcuts
nnoremap <Leader>gf :GFiles<CR>
nnoremap <Leader>ag :Ag 
nnoremap <Leader>bb :Buffers<CR>
nnoremap <Leader>cd :lcd %:p:h<CR>:pwd<CR>
" nmap <silent> <Leader>hh :call CocAction('doHover')<CR>
" nmap <silent> <Leader>gg <Plug>(coc-definition)
" nmap <silent> <Leader>rf <Plug>(coc-refactor)
nnoremap ; :
nmap <ESC>	:noh<CR>

" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? "\<C-n>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" function! s:check_back_space() abort
"   let col = col('.') - 1
"   return !col || getline('.')[col - 1]  =~# '\s'
" endfunction

" " Use <c-space> to trigger completion.
" inoremap <silent><expr> <c-space> coc#refresh()

" " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" " position. Coc only does snippet and additional edit on confirm.
" " <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
" if exists('*complete_info')
"   inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
" else
"   inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" endif
