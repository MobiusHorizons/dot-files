# Dot Files

This repo contains the configuration and setup files necessary for setting up a new system the way I like.
Eeah subdirectory has an install script that will install that subcomponent.
in the root directory there is an install script that can be used to install any number of subcomponents by name

````sh
./install zsh vim
````

this script can also be invoked with the `--all` flag in order to install all modules.
