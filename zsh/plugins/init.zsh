#!/usr/bin/env zsh

ZFUNCTIONS="${HOME}/.config/zsh/zfunctions"
PLUGINS="${HOME}/.config/zsh/plugins"

fpath=("$ZFUNCTIONS" $fpath)

plugins::pure::install() {
	ln -s "${PLUGINS}/pure/async.zsh"  "${ZFUNCTIONS}/async"
	ln -s "${PLUGINS}/pure/prompt.zsh" "${ZFUNCTIONS}/prompt_pure_setup"
}

plugins::pure() {
	if [ ! -L "${ZFUNCTIONS}/prompt_pure_setup" ]; then
		plugins::pure::install
	fi
	source "${PLUGINS}/pure/pure.plugin.zsh"
}


_register_plugin() {
	PLUGIN="$1"
	if [ -z $PLUGIN ]; then
		return 1; 
	fi
	NAME="$(basename $PLUGIN .plugin.zsh)"
	DIR="$(dirname $PLUGIN)"

	# load function
	declare -f -F plugins::${NAME} > /dev/null; exists=$?
	if [ $exists -ne 0 ]; then
		body="$(echo "plugins::${NAME}() { source" '"'"${PLUGIN}"'"' "}")"
		eval $body
	fi

	# update function
	declare -f -F plugins::${NAME}::update > /dev/null; exists=$?
	if [ $exists -ne 0 ]; then
		body="$(echo "plugins::${NAME}::update(){" $'\n'\
			pushd \""$DIR\""                   $'\n'\
			git pull                           $'\n'\
			popd                               $'\n'\
			plugins::${NAME}                   $'\n'\
		"}")"
		eval "$body"
	fi
}

plugins::init() {
	for plugin in ${PLUGINS}/*/*.plugin.zsh ; do 
		_register_plugin "$plugin"
	done
}

plugins::init

